#!/bin/bash

set -xe

softhsm2-util --init-token --free --label "token-label" --so-pin mysecret1 --pin mysecret1

softhsm2-util --show-slots

pkcs11-tool --module /usr/lib/softhsm/libsofthsm2.so --login --login-type so --so-pin mysecret1 --init-pin --new-pin mysecret1

# create a public-private key pair. 
pkcs11-tool --module /usr/lib/softhsm/libsofthsm2.so -l --token-label token-label -k --key-type rsa:2048      --usage-sign --id 1002 --label rsatest     --pin mysecret1

# extract the public key (https://xn--verschlsselt-jlb.it/export-a-rsa-ecc-public-key-with-opensc-pkcs11-tool/)
pkcs11-tool --modul /usr/lib/softhsm/libsofthsm2.so --id 1002 --read-object --type pubkey -o rsa.der

# The newly generated key can be used to sign some data:
echo "Hello World" >text.txt
pkcs11-tool --module /usr/lib/softhsm/libsofthsm2.so --login --pin mysecret1 --sign --id 1002 -m SHA512-RSA-PKCS   --input text.txt --output rsa.signature

# Validate
pkcs11-tool --module /usr/lib/softhsm/libsofthsm2.so --id 1002  --verify -m SHA512-RSA-PKCS   --input-file text.txt --signature-file  rsa.signature

# do it without softhsm online 
openssl dgst -verify rsa.der            -sha512 -keyform DER -signature rsa.signature text.txt

