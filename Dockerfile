FROM ubuntu:22.04
USER root
RUN apt update
# libp11-dev does not seem to be used
# pkg-config does not seem nessecary - or it is already there
# install gnutls-bin  'p11tool'
RUN apt install -y opensc git build-essential automake libtool libengine-pkcs11-openssl libp11-kit-dev libp11-dev gnutls-bin libcppunit-dev libssl-dev softhsm

COPY openssl.cnf /etc/ssl/openssl.cnf

# https://github.com/OpenSC/libp11/blob/master/README.md#using-the-engine-from-the-command-line
# To verify that the engine is properly operating you can use the following example.
# Note failure will not break docker build
RUN openssl engine pkcs11 -t

RUN pkcs11-tool --module /usr/lib/softhsm/libsofthsm2.so --list-mechanisms
