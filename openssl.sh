#!/bin/bash -ex
id

echo "Hello World"  > openssldemo.txt

# https://wiki.openssl.org/index.php/Command_Line_Elliptic_Curve_Operations#Generating_EC_Keys_and_Parameters
openssl ecparam -name prime256v1 -out openssl_prime256v1.pem
openssl ecparam -in openssl_prime256v1.pem -genkey -noout -out openssl_prime256v1-key.pem
openssl ec -in openssl_prime256v1-key.pem -pubout -out openssl_prime256v1-pub.pem

openssl ecparam -in prime256v1.pem -text -noout
openssl ecparam -in prime256v1.pem -text -param_enc explicit -noout

#openssl genrsa -out opensslkey.pem 4096
#openssl rsa -in opensslkey.pem -pubout > opensslkey.pub
#openssl rsa -inform PEM -pubin -in rsa.pem -text -noout


openssl dgst -sign openssl_prime256v1-key.pem -sha256 -keyform PEM -out openssldemo.txt.sign -binary openssldemo.txt

openssl dgst -verify openssl_prime256v1-pub.pem -sha256 -keyform PEM -signature openssldemo.txt.sign -binary openssldemo.txt
